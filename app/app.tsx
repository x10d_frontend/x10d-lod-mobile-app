/**
 * Welcome to the main entry point of the app. In this file, we'll
 * be kicking off our app or storybook.
 *
 * Most of this file is boilerplate and you shouldn't need to modify
 * it very often. But take some time to look through and understand
 * what is going on here.
 *
 * The app navigation resides in ./app/navigation, so head over there
 * if you're interested in adding screens and navigators.
 */
import './utils/ignore-warnings'
import React, { useState, useEffect, useRef } from 'react'
import { NavigationContainerRef } from '@react-navigation/native'
import * as SplashScreen from 'expo-splash-screen'
import {
    SafeAreaProvider,
    initialWindowSafeAreaInsets,
} from 'react-native-safe-area-context'
import {
    useBackButtonHandler,
    RootNavigator,
    canExit,
    setRootNavigation,
} from './navigation'
import { RootStore, RootStoreProvider, setupRootStore } from './models'

// This puts screens in a native ViewController or Activity. If you want fully native
// stack navigation, use `createNativeStackNavigator` in place of `createStackNavigator`:
// https://github.com/kmagiera/react-native-screens#using-native-stack-navigator
import { enableScreens } from 'react-native-screens'
enableScreens()

/**
 * This is the root component of our app.
 */
function App() {
    const navigationRef = useRef<NavigationContainerRef>()
    const [rootStore, setRootStore] = useState<RootStore | undefined>(undefined)

    setRootNavigation(navigationRef)
    useBackButtonHandler(navigationRef, canExit)

    useEffect(() => {
        ;(async () => {
            await SplashScreen.preventAutoHideAsync()

            const store = await setupRootStore()
            setRootStore(store)

            await SplashScreen.hideAsync()
        })()
    }, [])

    if (!rootStore) return null

    return (
        <RootStoreProvider value={rootStore}>
            <SafeAreaProvider
                initialSafeAreaInsets={initialWindowSafeAreaInsets}
            >
                <RootNavigator ref={navigationRef} />
            </SafeAreaProvider>
        </RootStoreProvider>
    )
}

export default App
