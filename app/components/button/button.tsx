import * as React from 'react'
import { TouchableOpacity } from 'react-native'
import { Text } from '..'
import { viewPresets, textPresets } from './button.presets'
import { ButtonProps } from './button.props'
import { mergeAll, flatten } from 'ramda'

export function Button(props: ButtonProps) {
    const {
        preset = 'primary',
        tx,
        text,
        style: styleOverride,
        textStyle: textStyleOverride,
        disabled,
        children,
        ...rest
    } = props

    const viewStyle = mergeAll(
        flatten([
            disabled ? viewPresets.disabled : viewPresets[preset],
            styleOverride,
        ]),
    )
    const textStyle = mergeAll(
        flatten([
            textPresets[preset] || textPresets.primary,
            textStyleOverride,
        ]),
    )

    const content = children || <Text tx={tx} text={text} style={textStyle} />

    return (
        <TouchableOpacity
            style={viewStyle}
            activeOpacity={0.8}
            hitSlop={{
                top: 10,
                right: 10,
                bottom: 10,
                left: 10,
            }}
            {...rest}
        >
            {content}
        </TouchableOpacity>
    )
}
