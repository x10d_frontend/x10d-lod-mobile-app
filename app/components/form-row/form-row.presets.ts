import { ViewStyle } from 'react-native'
import { color, spacing } from '../../theme'

/**
 * The size of the border radius.
 */
const RADIUS = 4

/**
 * The default style of the container.
 */
const ROOT: ViewStyle = {
    minHeight: 48,
    borderWidth: 1,
    borderColor: '#E0E0E0',
    paddingTop: spacing[1],
    paddingBottom: spacing[2],
    paddingHorizontal: spacing[3],
}

/**
 * What each of the presets look like.
 */
export const PRESETS = {
    /**
     * Rounded borders on the the top only.
     */
    top: {
        ...ROOT,
        borderTopLeftRadius: RADIUS,
        borderTopRightRadius: RADIUS,
        borderBottomWidth: 0,
    },
    /**
     * No rounded borders.
     */
    middle: {
        ...ROOT,
        borderBottomWidth: 0,
    },

    bottom: {
        borderWidth: 0,
        borderBottomWidth: 1,
        borderBottomColor: color.line,
        paddingBottom: spacing[2],
    },
    /**
     * Rounded borders everywhere.
     */
    soloRound: {
        ...ROOT,
        borderRadius: RADIUS,
    },
    /**
     * Straight borders everywhere.
     */
    soloStraight: {
        ...ROOT,
    },
    /**
     * Transparent borders useful to keep things lined up.
     */
    clear: {
        ...ROOT,
        borderColor: color.transparent,
    },
}

/**
 * The names of the presets supported by FormRow.
 */
export type FormRowPresets = keyof typeof PRESETS
