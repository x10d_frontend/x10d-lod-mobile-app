export const icons = {
    back: require('./arrow-left.png'),
    bullet: require('./bullet.png'),
    check: require('./check.png'),
    star: require('./star.png'),
    'chevron-left': require('./chevron-left.png'),
}

export type IconTypes = keyof typeof icons
