import { ViewStyle, TextInputProps } from 'react-native'

export interface InputProps extends TextInputProps {
    value: string

    onChangeText: (value: string) => void

    /**
     * An optional style override useful for padding & margin.
     */
    style?: ViewStyle | ViewStyle[]
}
