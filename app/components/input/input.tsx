import * as React from 'react'
import { TextInput } from 'react-native'
import { InputProps } from './input.props'
import { color, spacing } from '../../theme'
import { flatten, mergeAll } from 'ramda'

const INPUT = {
    minHeight: 48,
    paddingVertical: spacing[2],
    paddingHorizontal: spacing[3],
    fontSize: 16,
    letterSpacing: 0.15,
    borderRadius: 4,
    borderWidth: 1,
    borderColor: color.palette.alto,
    backgroundColor: color.palette.alabaster,
}

export function Input(props: InputProps) {
    const { value, onChangeText, style: styleOverride, ...rest } = props

    const viewStyle = mergeAll(flatten([INPUT, styleOverride]))

    return (
        <TextInput
            value={props.value}
            placeholder="Номер телефона"
            onChangeText={props.onChangeText}
            style={viewStyle}
            {...rest}
        />
    )
}
