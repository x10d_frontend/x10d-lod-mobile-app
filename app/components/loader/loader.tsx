import React from 'react'
import { MaterialIndicator } from 'react-native-indicators'
import { color } from '../../theme'

const LOADER = {
    flex: 0,
}

export const Loader = (props) => {
    return (
        <MaterialIndicator
            style={{
                ...LOADER,
                ...props.style,
            }}
            color={props.color || color.palette.aquamarineBlue}
            size={props.size || 44}
        />
    )
}
