import React from 'react'
import RNPickerSelect from 'react-native-picker-select'

const PLACEHOLDER = {
    flex: 1,
    fontSize: 16,
    letterSpacing: 0.15,
}

export const Picker = (props) => {
    return (
        <RNPickerSelect
            value={props.value}
            onValueChange={props.onValueChange}
            items={props.items}
            doneText="Готово"
            placeholder={{
                label: 'Выбрать',
                value: null,
            }}
            style={{
                inputIOS: PLACEHOLDER,
            }}
        />
    )
}
