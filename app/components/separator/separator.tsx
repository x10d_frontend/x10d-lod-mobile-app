import React from 'react'
import { View } from 'react-native'
import { color } from '../../theme'

const SEPARATOR = {
    width: '100%',
    height: 1,
    backgroundColor: color.line,
}

export const Separator = () => {
    return <View style={SEPARATOR} />
}
