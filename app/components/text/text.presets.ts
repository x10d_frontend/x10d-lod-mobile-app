import { TextStyle } from 'react-native'
import { color, typography } from '../../theme'

/**
 * All text will start off looking like this.
 */
const BASE: TextStyle = {
    fontFamily: typography.primary,
    color: color.text,
    fontSize: 15,
}

/**
 * All the variations of text styling within the app.
 *
 * You want to customize these to whatever you need in your app.
 */
export const presets = {
    /**
     * The default text styles.
     */
    default: BASE,

    /**
     * A bold version of the default text.
     */
    bold: { ...BASE, fontWeight: 'bold' } as TextStyle,

    /**
     * Large headers.
     */
    header: {
        ...BASE,
        fontSize: 24,
        fontWeight: '700',
        letterSpacing: 0.15,
    } as TextStyle,

    /**
     * Field labels that appear on forms above the inputs.
     */
    fieldLabel: { ...BASE, fontSize: 13, color: color.dim } as TextStyle,

    listPrimary: {
        ...BASE,
        fontSize: 16,
        lineHeight: 19,
        letterSpacing: 0.15,
        fontWeight: '400',
        color: color.palette.black,
    } as TextStyle,
    listSecondary: {
        ...BASE,
        color: '#B1B1B1',
        fontWeight: '500',
        letterSpacing: 0.25,
        fontSize: 14,
        lineHeight: 20,
    } as TextStyle,
}

/**
 * A list of preset names.
 */
export type TextPresets = keyof typeof presets
