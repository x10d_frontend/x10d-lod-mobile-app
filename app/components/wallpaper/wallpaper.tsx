import React from 'react'
import { Image } from 'react-native'
import { presets } from './wallpaper.presets'
import { WallpaperProps } from './wallpaper.props'

const defaultImage = require('./bg.png')

export function Wallpaper(props: WallpaperProps) {
    const { preset = 'stretch', style: styleOverride, backgroundImage } = props

    const presetToUse = presets[preset] || presets.stretch
    const style = { ...presetToUse, ...styleOverride }

    const source = backgroundImage || defaultImage

    return <Image source={source} style={style} />
}
