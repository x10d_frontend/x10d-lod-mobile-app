import { Instance, SnapshotOut, types, flow } from 'mobx-state-tree'
import { withRootEnvironment, withRootStore, withStatus } from '..'

export const AuthStoreModel = types
    .model('AuthStore')
    .props({
        firebaseUser: types.frozen(null),
        accessToken: types.maybeNull(types.string),
        userData: types.maybeNull(
            types.model('UserData', {
                _id: types.string,
                firstName: types.maybe(types.string),
                middleName: types.maybe(types.string),
                lastName: types.maybe(types.string),
                birthDate: types.maybe(types.string),
                memberId: types.maybe(types.string),
                phone: types.maybe(types.string),
                directions: types.maybe(types.array(types.string)),
                educationLevels: types.maybe(types.array(types.string)),
                goals: types.maybe(types.array(types.string)),
                profiles: types.maybe(types.array(types.string)),
            }),
        ),
    })
    .views((self) => ({
        get isUserSignedIn() {
            return self.accessToken !== null
        },

        get userInfo() {
            return self.userData
        },
    }))
    .extend(withRootEnvironment)
    .extend(withRootStore)
    .extend(withStatus)
    .actions((self) => ({
        handleAccessToken: flow(function* (accessToken: string) {
            self.environment.api.setAuthHeader(accessToken)

            const userData = yield self.environment.api.fetchUserData()

            self.setUserData(userData)

            self.accessToken = accessToken
        }),

        handleUser: flow(function* () {
            if (self.firebaseUser !== null) {
                const accessToken = yield self.environment.api.login(
                    self.firebaseUser.uid,
                )

                if (!accessToken) {
                    throw new Error('User does not exist')
                }

                yield self.handleAccessToken(accessToken)
            }
        }),

        activateUser: flow(function* (inviteCode: string) {
            yield self.environment.api.activateUser(
                inviteCode,
                self.firebaseUser.uid,
                self.environment.notifications.token,
            )

            const accessToken = yield self.environment.api.login(
                self.firebaseUser.uid,
            )

            self.handleAccessToken(accessToken)
        }),

        addUserToEvent: flow(function* (eventId: string) {
            self.setStatus('pending')

            const updatedEvent = yield self.environment.api.addUserToEvent(
                eventId,
            )

            self.rootStore.eventsStore.mergeItemById(eventId, updatedEvent)

            self.setStatus('done')
        }),

        updateUserInfo: flow(function* (data: any) {
            self.setStatus('pending')

            const updatedUserData = yield self.environment.api.updateUserInfo({
                _id: self.userData._id,
                ...data,
            })

            self.setUserData(updatedUserData)

            self.setStatus('done')
        }),

        setUserData: (data) => {
            self.userData = data
        },

        setFirebaseUser: (data) => {
            self.firebaseUser = data
        },
    }))

export interface AuthStore extends Instance<typeof AuthStoreModel> {}

export interface AuthStoreSnapshot extends SnapshotOut<typeof AuthStoreModel> {}
