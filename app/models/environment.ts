import { Api } from '../services/api'
import { Notifications } from '../services/notifications'
import { Firebase } from '../services/firebase'
import { RootNavigation } from '../navigation'

let ReactotronDev
if (__DEV__) {
    const { Reactotron } = require('../services/reactotron')
    ReactotronDev = Reactotron
}

/**
 * The environment is a place where services and shared dependencies between
 * models live.  They are made available to every model via dependency injection.
 */
export class Environment {
    api: Api

    notifications: Notifications

    firebase: Firebase

    navigation: typeof RootNavigation

    reactotron: typeof ReactotronDev

    constructor() {
        // create each service
        if (__DEV__) {
            // dev-only services
            this.reactotron = new ReactotronDev()
        }
        this.api = new Api()
        this.navigation = RootNavigation
        this.notifications = new Notifications(this.navigation)
        this.firebase = new Firebase()
    }

    async setup() {
        // allow each service to setup
        if (__DEV__) {
            await this.reactotron.setup()
        }
        await this.api.setup()
        await this.notifications.setup()
        await this.firebase.setup()
    }
}
