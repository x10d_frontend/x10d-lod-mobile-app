import { Instance, SnapshotOut, types } from 'mobx-state-tree'

export const EventModel = types
    .model('Event')
    .props({
        _id: types.string,
        title: types.string,
        description: types.string,
        address: types.string,
        dateStart: types.string,
        dateEnd: types.string,
        isMember: types.boolean,
    })
    .views((self) => ({}))
    .actions((self) => ({}))

export interface Event extends Instance<typeof EventModel> {}

export interface EventSnapshot extends SnapshotOut<typeof EventModel> {}
