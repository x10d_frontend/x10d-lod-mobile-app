import { Instance, SnapshotOut, types, flow } from 'mobx-state-tree'
import { EventModel } from './event'
import { withRootEnvironment, withRootStore, withStatus } from '..'

export const EventsStoreModel = types
    .model('EventsStore')
    .props({
        items: types.array(EventModel),
    })
    .views((self) => ({}))
    .extend(withRootEnvironment)
    .extend(withRootStore)
    .extend(withStatus)
    .actions((self) => ({
        fetchItems: flow(function* () {
            self.setStatus('pending')

            self.items = yield self.environment.api.fetchEvents(
                self.rootStore.authStore.userData.memberId,
            )

            self.setStatus('done')
        }),

        mergeItemById(id: string, data: any) {
            const itemIdx = self.items.findIndex((item) => item._id === id)

            self.items[itemIdx] = {
                ...self.items[itemIdx],
                ...data,
            }
        },

        getItemById(id: string) {
            return self.items.find((item) => item._id === id)
        },
    }))

export interface EventsStore extends Instance<typeof EventsStoreModel> {}

export interface EventsStoreSnapshot
    extends SnapshotOut<typeof EventsStoreModel> {}
