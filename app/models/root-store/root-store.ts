import { flow, Instance, SnapshotOut, types } from 'mobx-state-tree'
import { AuthStoreModel } from '../auth-store/auth-store'
import { EventsStoreModel } from '../events-store/events-store'
import { withRootEnvironment } from '..'

export const RootStoreModel = types
    .model('RootStore')
    .props({
        authStore: AuthStoreModel,
        eventsStore: EventsStoreModel,
        dictionaries: types.frozen(null),
    })
    .extend(withRootEnvironment)
    .actions((self) => ({
        setup: flow(function* () {
            try {
                self.dictionaries = yield self.environment.api.fetchDictionaries()

                yield self.authStore.handleUser()
            } catch (e) {
                if (e.message === 'User does not exist') {
                    return
                }

                throw e
            }
        }),
    }))

export interface RootStore extends Instance<typeof RootStoreModel> {}

export interface RootStoreSnapshot extends SnapshotOut<typeof RootStoreModel> {}
