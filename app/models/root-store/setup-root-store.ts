import { RootStoreModel, RootStore } from './root-store'
import { AuthStoreModel } from '../auth-store/auth-store'
import { EventsStoreModel } from '../events-store/events-store'
import { Environment } from '../environment'

export async function createEnvironment() {
    const env = new Environment()
    await env.setup()
    return env
}

export async function setupRootStore() {
    const env = await createEnvironment()

    const rootStore: RootStore = RootStoreModel.create(
        {
            authStore: AuthStoreModel.create({
                firebaseUser: env.firebase.currentUser,
                accessToken: null,
                userData: null,
            }),
            eventsStore: EventsStoreModel.create({
                items: [],
            }),
        },
        env,
    )

    await rootStore.setup()

    // reactotron logging
    if (__DEV__) {
        env.reactotron.setRootStore(rootStore, {})
    }

    return rootStore
}
