import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import { TabBar, EventDetailsScreen } from '../screens'

export type PrimaryParamList = {
    tabBar: undefined
    eventDetails: {
        id: string
    }
}

// Documentation: https://reactnavigation.org/docs/stack-navigator/
const Stack = createStackNavigator<PrimaryParamList>()

export const PrimaryNavigator = () => {
    return (
        <Stack.Navigator
            screenOptions={{
                headerShown: false,
                gestureEnabled: true,
            }}
        >
            <Stack.Screen name="tabBar" component={TabBar} />
            <Stack.Screen name="eventDetails" component={EventDetailsScreen} />
        </Stack.Navigator>
    )
}

/**
 * A list of routes from which we're allowed to leave the app when
 * the user presses the back button on Android.
 *
 * Anything not on this list will be a standard `back` action in
 * react-navigation.
 *
 * `canExit` is used in ./app/app.tsx in the `useBackButtonHandler` hook.
 */
const exitRoutes = ['login']
export const canExit = (routeName: string) => exitRoutes.includes(routeName)
