import React from 'react'
import {
    NavigationContainer,
    NavigationContainerRef,
} from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import { PrimaryNavigator } from './primary-navigator'
import { observer } from 'mobx-react-lite'
import { useStores } from '../models'
import { LoginScreen, InvitationScreen } from '../screens'

/**
 * This type allows TypeScript to know what routes are defined in this navigator
 * as well as what properties (if any) they might take when navigating to them.
 *
 * We recommend using MobX-State-Tree store(s) to handle state rather than navigation params.
 *
 * For more information, see this documentation:
 *   https://reactnavigation.org/docs/params/
 *   https://reactnavigation.org/docs/typescript#type-checking-the-navigator
 */
export type RootParamList = {
    login: undefined
    invitation: undefined
    primaryStack: undefined
}

const Stack = createStackNavigator<RootParamList>()

const RootStack = observer(() => {
    const { authStore } = useStores()

    return (
        <Stack.Navigator
            screenOptions={{
                headerShown: false,
                gestureEnabled: true,
            }}
        >
            {authStore.isUserSignedIn ? (
                <Stack.Screen
                    name="primaryStack"
                    component={PrimaryNavigator}
                    options={{
                        headerShown: false,
                    }}
                />
            ) : (
                <>
                    <Stack.Screen name="login" component={LoginScreen} />
                    <Stack.Screen
                        name="invitation"
                        component={InvitationScreen}
                    />
                </>
            )}
        </Stack.Navigator>
    )
})

export const RootNavigator = React.forwardRef<
    NavigationContainerRef,
    Partial<React.ComponentProps<typeof NavigationContainer>>
>((props, ref) => {
    return (
        <NavigationContainer {...props} ref={ref}>
            <RootStack />
        </NavigationContainer>
    )
})

RootNavigator.displayName = 'RootNavigator'
