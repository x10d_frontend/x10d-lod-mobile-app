import React from 'react'
import { View, ViewStyle } from 'react-native'
import { useNavigation } from '@react-navigation/native'
import { observer } from 'mobx-react-lite'
import {
    Screen,
    Wallpaper,
    Text,
    Header,
    Button,
    Loader,
} from '../../components'
import { color, spacing } from '../../theme'
import { useStores } from '../../models'
import { eventDate } from '../../utils/date'
import { useSafeAreaInsets } from 'react-native-safe-area-context'

const FULL: ViewStyle = { flex: 1 }
const CONTAINER: ViewStyle = {
    flex: 1,
    backgroundColor: color.transparent,
    paddingHorizontal: spacing[4],
}

export const EventDetailsScreen = observer(({ route }) => {
    const navigation = useNavigation()
    const insets = useSafeAreaInsets()
    const { authStore, eventsStore } = useStores()

    const event = eventsStore.getItemById(route.params.id)
    const addUserToEvent = async () => {
        await authStore.addUserToEvent(event._id)
    }

    return (
        <View style={FULL}>
            <Wallpaper />

            <Screen
                style={CONTAINER}
                preset="scroll"
                statusBar="dark-content"
                backgroundColor={color.transparent}
            >
                <Header
                    leftIcon="chevron-left"
                    headerText="Информация о событии"
                    onLeftPress={navigation.goBack}
                />

                <Text
                    preset="header"
                    text={event.title}
                    style={{
                        fontSize: 20,
                    }}
                />

                <Text
                    style={{ marginTop: spacing[2] }}
                    preset="listSecondary"
                    text={event.address}
                />
                <Text
                    preset="listSecondary"
                    text={`с ${eventDate(event.dateStart)} по ${eventDate(
                        event.dateEnd,
                    )}`}
                />

                <Text
                    preset="listSecondary"
                    style={{
                        marginTop: spacing[2],
                        color: color.text,
                        fontWeight: '400',
                    }}
                    text={event.description}
                />

                {event.isMember ? (
                    <Text
                        style={{
                            fontWeight: '500',
                            fontSize: 20,
                            letterSpacing: 1,
                            alignSelf: 'center',
                            marginTop: 'auto',
                            marginBottom: insets.bottom,
                            color: color.palette.aquamarineBlue,
                        }}
                        text="Вы участвуете в событии!"
                    />
                ) : (
                    <Button
                        onPress={addUserToEvent}
                        style={{
                            marginTop: 'auto',
                            marginBottom: insets.bottom,
                        }}
                        text={
                            authStore.status === 'pending'
                                ? null
                                : 'Участвовать'
                        }
                        disabled={authStore.status === 'pending'}
                        children={
                            authStore.status === 'pending' ? (
                                <Loader size={22} color={color.palette.white} />
                            ) : null
                        }
                    />
                )}
            </Screen>
        </View>
    )
})
