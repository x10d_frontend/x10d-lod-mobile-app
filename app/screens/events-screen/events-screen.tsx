import React, { useEffect } from 'react'
import { prop } from 'ramda'
import {
    FlatList,
    View,
    ViewStyle,
    TouchableOpacity,
    RefreshControl,
} from 'react-native'
import { useNavigation } from '@react-navigation/native'
import { observer } from 'mobx-react-lite'
import { Screen, Text, Wallpaper, Separator } from '../../components'
import { color, spacing } from '../../theme'
import { useStores } from '../../models'
import { eventDate } from '../../utils/date'

const FULL: ViewStyle = { flex: 1 }
const CONTAINER: ViewStyle = {
    backgroundColor: color.transparent,
    paddingHorizontal: spacing[6],
}
const LOADER = {
    textAlign: 'center',
    fontSize: 16,
    fontWeight: '400',
    letterSpacing: 0.15,
    marginTop: spacing[8],
}
const LIST_ITEM = {
    paddingVertical: spacing[3],
}

export const EventsScreen = observer(() => {
    const navigation = useNavigation()
    const { eventsStore } = useStores()

    useEffect(() => {
        ;(async function () {
            await eventsStore.fetchItems()
        })()
    }, [])

    const renderItem = ({ item }) => (
        <TouchableOpacity
            onPress={() => {
                navigation.navigate('eventDetails', {
                    id: item._id,
                })
            }}
            activeOpacity={0.8}
            style={LIST_ITEM}
        >
            <Text
                preset="listSecondary"
                text={`${eventDate(item.dateStart)} - ${eventDate(
                    item.dateEnd,
                )}`}
            />
            <Text
                style={{ marginTop: spacing[1] }}
                preset="listPrimary"
                text={item.title}
            />
        </TouchableOpacity>
    )

    const renderStub = () =>
        eventsStore.status === 'pending' ? null : (
            <Text text="Нет данных" style={LOADER} />
        )

    return (
        <View style={FULL}>
            <Wallpaper />

            <Screen
                style={CONTAINER}
                preset="fixed"
                statusBar="dark-content"
                backgroundColor={color.transparent}
            >
                <FlatList
                    data={eventsStore.items}
                    renderItem={renderItem}
                    ListEmptyComponent={renderStub}
                    refreshControl={
                        <RefreshControl
                            tintColor={color.palette.aquamarineBlue}
                            colors={[color.palette.aquamarineBlue]}
                            onRefresh={eventsStore.fetchItems}
                            refreshing={eventsStore.status === 'pending'}
                        />
                    }
                    ItemSeparatorComponent={Separator}
                    keyExtractor={prop('_id')}
                />
            </Screen>
        </View>
    )
})
