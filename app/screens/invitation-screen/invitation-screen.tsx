import React from 'react'
import { View, ViewStyle } from 'react-native'
import { observer, useLocalStore } from 'mobx-react-lite'
import { toUpper } from 'ramda'
import {
    Button,
    Input,
    Loader,
    Screen,
    Text,
    Wallpaper,
} from '../../components'
import { color, spacing } from '../../theme'
import { useStores } from '../../models'

const FULL: ViewStyle = { flex: 1 }
const CONTAINER: ViewStyle = {
    backgroundColor: color.transparent,
    paddingHorizontal: spacing[4],
}
const BUTTON = {
    marginTop: spacing[2],
}
const INPUT = {
    marginTop: spacing[4],
}
const TITLE = {
    textAlign: 'center',
    marginTop: spacing[5],
}
const LOADER = {
    marginTop: spacing[8],
}

export const InvitationScreen = observer(() => {
    const { authStore } = useStores()
    const state = useLocalStore(() => ({
        inviteCode: '',
        isLoading: false,

        setInviteCode: (code) => {
            state.inviteCode = toUpper(code)
        },

        activateUser: async () => {
            state.isLoading = true

            await authStore.activateUser(state.inviteCode)

            state.isLoading = false
        },
    }))

    return (
        <View style={FULL}>
            <Wallpaper />

            <Screen
                style={CONTAINER}
                preset="scroll"
                statusBar="dark-content"
                backgroundColor={color.transparent}
            >
                {state.isLoading ? (
                    <Loader style={LOADER} />
                ) : (
                    <>
                        {/* @ts-ignore */}
                        <Text style={TITLE} text="Активация" preset="header" />

                        <Input
                            style={INPUT}
                            value={state.inviteCode}
                            placeholder="Введите пригласительный код"
                            onChangeText={state.setInviteCode}
                            autoFocus
                        />

                        <Button
                            onPress={state.activateUser}
                            style={BUTTON}
                            text="Отправить"
                        />
                    </>
                )}
            </Screen>
        </View>
    )
})
