import React, { useRef } from 'react'
import * as firebase from 'firebase'
import { useNavigation } from '@react-navigation/native'
import { View, ViewStyle, StatusBar } from 'react-native'
import { FirebaseRecaptchaVerifierModal } from 'expo-firebase-recaptcha'
import { observer, useLocalStore } from 'mobx-react-lite'
import {
    Screen,
    Wallpaper,
    Button,
    Input,
    Loader,
    Text,
} from '../../components'
import { useStores } from '../../models'
import { formatPhoneNumberAsYouType } from '../../utils/phone-number'
import { color, spacing } from '../../theme'
import { pipe, prop } from 'ramda'

const FULL: ViewStyle = { flex: 1 }
const CONTAINER: ViewStyle = {
    backgroundColor: color.transparent,
    paddingHorizontal: spacing[4],
}
const BUTTON = {
    marginTop: spacing[2],
}
const INPUT = {
    marginTop: spacing[4],
}
const TITLE = {
    textAlign: 'center',
    marginTop: spacing[5],
}
const LOADER = {
    marginTop: spacing[8],
}

export const LoginScreen = observer(() => {
    const { authStore } = useStores()
    const navigation = useNavigation()
    const recaptchaVerifier = useRef(null)

    const state = useLocalStore(() => ({
        phoneNumber: '+7',
        smsCode: '',
        verificationId: null,

        asyncState: null as null | 'SENDING_CODE' | 'CONFIRMING_CODE',

        setPhoneNumber: (number: string) => {
            state.phoneNumber = formatPhoneNumberAsYouType(number)
        },

        setSmsCode: (code: string) => {
            state.smsCode = code.replace(/[^\d]/g, '')
        },

        setVerificationId: (id: string) => {
            state.verificationId = id
        },

        sendVerification: () => {
            state.asyncState = 'SENDING_CODE'

            new firebase.auth.PhoneAuthProvider()
                .verifyPhoneNumber(state.phoneNumber, recaptchaVerifier.current)
                .then((id) => {
                    state.setVerificationId(id)

                    state.asyncState = null

                    StatusBar.setBarStyle('dark-content')
                })
        },

        confirmCode: () => {
            state.asyncState = 'CONFIRMING_CODE'

            const credential = firebase.auth.PhoneAuthProvider.credential(
                state.verificationId,
                state.smsCode,
            )
            firebase
                .auth()
                .signInWithCredential(credential)
                .then(
                    pipe(
                        prop('user'),
                        authStore.setFirebaseUser,
                        authStore.handleUser,
                    ),
                )
                .catch((e) => {
                    navigation.reset({
                        routes: [{ name: 'invitation' }],
                    })
                })
        },

        get screenTitle() {
            let title = ''
            if (!state.asyncState && !state.verificationId) {
                title = 'Вход'
            } else if (state.asyncState === 'SENDING_CODE') {
                title = 'Отправка кода...'
            } else if (!state.asyncState && state.verificationId) {
                title = 'SMS подтверждение'
            } else if (state.asyncState === 'CONFIRMING_CODE') {
                title = 'Проверка кода...'
            }

            return title
        },
    }))

    return (
        <View style={FULL}>
            <Wallpaper />

            <FirebaseRecaptchaVerifierModal
                ref={recaptchaVerifier}
                // @ts-ignore
                firebaseConfig={firebase.app().options}
                title="CAPTCHA"
                cancelLabel="Назад"
            />

            <Screen
                style={CONTAINER}
                preset="scroll"
                backgroundColor={color.transparent}
                statusBar="dark-content"
            >
                {/* @ts-ignore */}
                <Text style={TITLE} text={state.screenTitle} preset="header" />

                {state.asyncState !== null ? (
                    <Loader style={LOADER} />
                ) : state.verificationId === null ? (
                    <>
                        <Input
                            style={INPUT}
                            value={state.phoneNumber}
                            placeholder="Номер телефона"
                            onChangeText={state.setPhoneNumber}
                            keyboardType="phone-pad"
                            autoCompleteType="tel"
                            autoFocus
                        />

                        <Button
                            onPress={state.sendVerification}
                            style={BUTTON}
                            text="Отправить код"
                        />
                    </>
                ) : (
                    <>
                        <Input
                            style={INPUT}
                            value={state.smsCode}
                            placeholder="Код подтверждения"
                            onChangeText={state.setSmsCode}
                            keyboardType="number-pad"
                            maxLength={6}
                            autoFocus
                        />
                        <Button
                            onPress={state.confirmCode}
                            style={BUTTON}
                            text="Подтвердить"
                        />
                    </>
                )}
            </Screen>
        </View>
    )
})
