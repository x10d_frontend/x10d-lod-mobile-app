import React from 'react'
import { TextInput, View, ViewStyle } from 'react-native'
import { observer, useLocalStore } from 'mobx-react-lite'
import { useStores } from '../../models'
import {
    Screen,
    Wallpaper,
    Picker,
    FormRow,
    Text,
    Loader,
    Button,
} from '../../components'
import { color, spacing } from '../../theme'

const FULL: ViewStyle = { flex: 1 }
const CONTAINER: ViewStyle = {
    backgroundColor: color.transparent,
    paddingHorizontal: spacing[4],
    paddingBottom: spacing[5],
}
const FORM_ROW_LABEL = {
    fontWeight: '500' as '500',
    fontSize: 14,
    lineHeight: 20,
    letterSpacing: 0.25,
    color: '#BBBEC2',
}
const TITLE = {
    marginTop: spacing[6],
}
const INPUT = {
    fontWeight: '400',
    fontSize: 16,
    letterSpacing: 0.15,
}
const FORM_ROW_INPUT = {
    marginTop: spacing[3],
}
const BUTTON = {
    marginTop: spacing[5],
}

export const ProfileScreen = observer(() => {
    const { authStore, dictionaries } = useStores()

    const state = useLocalStore(() => ({
        direction:
            authStore.userInfo.directions.length > 0
                ? authStore.userInfo.directions[0]
                : null,
        setDirection: (direction) => {
            state.direction = direction
        },
        get directions() {
            return dictionaries.directions.map((d) => ({
                label: d.name,
                value: d._id,
                key: d._id,
            }))
        },

        profile:
            authStore.userInfo.profiles.length > 0
                ? authStore.userInfo.profiles[0]
                : null,
        setProfile: (profile) => {
            state.profile = profile
        },
        get profiles() {
            return dictionaries.profiles.map((p) => ({
                label: p.name,
                value: p._id,
                key: p._id,
            }))
        },

        educationLevel:
            authStore.userInfo.educationLevels.length > 0
                ? authStore.userInfo.educationLevels[0]
                : null,
        setEducationLevel: (educationLevel) => {
            state.educationLevel = educationLevel
        },
        get educationLevels() {
            return dictionaries.educationLevels.map((l) => ({
                label: l.name,
                value: l._id,
                key: l._id,
            }))
        },

        goal:
            authStore.userInfo.goals.length > 0
                ? authStore.userInfo.goals[0]
                : null,
        setGoal: (goal) => {
            state.goal = goal
        },
        get goals() {
            return dictionaries.goals.map((g) => ({
                label: g.name,
                value: g._id,
                key: g._id,
            }))
        },

        user: {
            firstName: authStore.userInfo.firstName,
            lastName: authStore.userInfo.lastName,
            middleName: authStore.userInfo.middleName,
        },
        setFirstName: (value) => {
            state.user.firstName = value
        },
        setLastName: (value) => {
            state.user.lastName = value
        },
        setMiddleName: (value) => {
            state.user.middleName = value
        },
        saveUserInfo: async () => {
            await authStore.updateUserInfo({
                firstName: state.user.firstName,
                lastName: state.user.lastName,
                middleName: state.user.middleName,
                directions: state.direction ? [state.direction] : [],
                educationLevels: state.educationLevel
                    ? [state.educationLevel]
                    : [],
                goals: state.goal ? [state.goal] : [],
                profiles: state.profile ? [state.profile] : [],
            })
        },
    }))

    return (
        <View style={FULL}>
            <Wallpaper />

            <Screen
                statusBar="dark-content"
                style={CONTAINER}
                preset="scroll"
                backgroundColor={color.transparent}
            >
                <Text preset="header" text="Общая информация" />
                <FormRow preset="soloRound" style={FORM_ROW_INPUT}>
                    <Text style={FORM_ROW_LABEL} text="Фамилия" />
                    <TextInput
                        value={state.user.lastName}
                        onChangeText={state.setLastName}
                        style={INPUT}
                    />
                </FormRow>
                <FormRow preset="soloRound" style={FORM_ROW_INPUT}>
                    <Text style={FORM_ROW_LABEL} text="Имя" />
                    <TextInput
                        value={state.user.firstName}
                        onChangeText={state.setFirstName}
                        style={INPUT}
                    />
                </FormRow>
                <FormRow preset="soloRound" style={FORM_ROW_INPUT}>
                    <Text style={FORM_ROW_LABEL} text="Отчество" />
                    <TextInput
                        value={state.user.middleName}
                        onChangeText={state.setMiddleName}
                        style={INPUT}
                    />
                </FormRow>
                <Text style={TITLE} preset="header" text="Интересы" />
                <FormRow style={FORM_ROW_INPUT} preset="soloRound">
                    <Text
                        preset="fieldLabel"
                        style={FORM_ROW_LABEL}
                        text="Направление"
                    />
                    <Picker
                        items={state.directions}
                        value={state.direction}
                        onValueChange={state.setDirection}
                    />
                </FormRow>
                <FormRow style={FORM_ROW_INPUT} preset="soloRound">
                    <Text
                        preset="fieldLabel"
                        style={FORM_ROW_LABEL}
                        text="Профиль"
                    />
                    <Picker
                        items={state.profiles}
                        value={state.profile}
                        onValueChange={state.setProfile}
                    />
                </FormRow>
                <FormRow style={FORM_ROW_INPUT} preset="soloRound">
                    <Text
                        preset="fieldLabel"
                        style={FORM_ROW_LABEL}
                        text="Образование"
                    />
                    <Picker
                        items={state.educationLevels}
                        value={state.educationLevel}
                        onValueChange={state.setEducationLevel}
                    />
                </FormRow>
                <FormRow style={FORM_ROW_INPUT} preset="soloRound">
                    <Text
                        preset="fieldLabel"
                        style={FORM_ROW_LABEL}
                        text="Цель"
                    />
                    <Picker
                        items={state.goals}
                        value={state.goal}
                        onValueChange={state.setGoal}
                    />
                </FormRow>

                <Button
                    onPress={state.saveUserInfo}
                    style={BUTTON}
                    text={authStore.status === 'pending' ? null : 'Сохранить'}
                    disabled={authStore.status === 'pending'}
                    children={
                        authStore.status === 'pending' ? (
                            <Loader size={22} color={color.palette.white} />
                        ) : null
                    }
                />
            </Screen>
        </View>
    )
})
