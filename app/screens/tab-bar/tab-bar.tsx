import React from 'react'
import { useSafeAreaInsets } from 'react-native-safe-area-context'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'

import { EventsScreen } from '..'
import { ProfileScreen } from '..'

import { Icon, Text } from '../../components'

import { color } from '../../theme'

const TAB_LABEL = {
    fontSize: 12,
    lineHeight: 16,
    letterSpacing: 0.4,
}

const TAB_ICON = {
    width: 24,
    height: 24,
}

const BottomTab = createBottomTabNavigator()

export const TabBar: React.FC = () => {
    const insets = useSafeAreaInsets()

    return (
        <BottomTab.Navigator
            screenOptions={({ route }) => ({
                tabBarIcon: ({ size, color }) => {
                    const icons = {
                        events: 'check',
                        profile: 'star',
                    }

                    return (
                        <Icon
                            style={{ ...TAB_ICON, tintColor: color }}
                            icon={icons[route.name]}
                        />
                    )
                },
                tabBarLabel: ({ color }) => {
                    const labels = {
                        events: 'События',
                        profile: 'Профиль',
                    }

                    return (
                        <Text
                            style={{ ...TAB_LABEL, color }}
                            text={labels[route.name]}
                        />
                    )
                },
            })}
            tabBarOptions={{
                activeTintColor: color.palette.white,
                inactiveTintColor: color.palette.shuttleGrey,
                style: {
                    height: 56 + insets.bottom,
                    elevation: 9,
                    backgroundColor: color.palette.mirage,
                    borderTopColor: color.palette.mirage,
                },
            }}
        >
            <BottomTab.Screen name="profile" component={ProfileScreen} />
            <BottomTab.Screen name="events" component={EventsScreen} />
        </BottomTab.Navigator>
    )
}
