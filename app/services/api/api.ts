import { ApisauceInstance, create, ApiResponse } from 'apisauce'
import { getGeneralApiProblem } from './api-problem'
import { ApiConfig, DEFAULT_API_CONFIG } from './api-config'
import * as Types from './api.types'

/**
 * Manages all requests to the API.
 */
export class Api {
    /**
     * The underlying apisauce instance which performs the requests.
     */
    apisauce: ApisauceInstance

    /**
     * Configurable options.
     */
    config: ApiConfig

    /**
     * Creates the api.
     *
     * @param config The configuration to use.
     */
    constructor(config: ApiConfig = DEFAULT_API_CONFIG) {
        this.config = config
    }

    setup() {
        // construct the apisauce instance
        this.apisauce = create({
            baseURL: this.config.url,
            timeout: this.config.timeout,
            headers: {
                Accept: 'application/json',
            },
        })
    }

    public setAuthHeader(accessToken: string) {
        this.apisauce.setHeader('Authorization', `Bearer ${accessToken}`)
    }

    public async activateUser(
        inviteCode: string,
        firebaseUid: string,
        pushToken: string,
    ): Promise<void> {
        await this.apisauce.post('/auth/activate', {
            inviteCode,
            firebaseUid,
            pushToken,
        })
    }

    public async addUserToEvent(eventId: string) {
        const response = await this.apisauce.get(
            `mobile/addStudentToContest/${eventId}`,
        )

        return response.data
    }

    public async login(firebaseUid: string): Promise<string | null> {
        const response = await this.apisauce.post<any, string>('/auth/login', {
            firebaseUid,
        })

        if (!response.ok) return null

        return response.data.access_token
    }

    public async fetchUserData() {
        const response = await this.apisauce.get('mobile/profile')

        return response.data
    }

    public async updateUserInfo(data) {
        const response = await this.apisauce.post(
            `/mobile/profile/update`,
            data,
        )

        return response.data
    }

    public async fetchEvents(memberId: string) {
        const response = await this.apisauce.get(
            `mobile/getRelevantContestsByUser/${memberId}`,
        )

        return response.data
    }

    public async fetchDictionaries() {
        const response = await this.apisauce.get('dictionaries/list')

        return response.data
    }
}
