import * as firebase from 'firebase'
import '@firebase/auth'

export class Firebase {
    currentUser: firebase.User

    async setup() {
        return new Promise((resolve) => {
            try {
                firebase.initializeApp({
                    apiKey: 'AIzaSyBNn82pgV8cDlmYoVI-3ev17PFFf4VCcQo',
                    authDomain: 'x10d-lod.firebaseapp.com',
                    databaseURL: 'https://x10d-lod.firebaseio.com',
                    projectId: 'x10d-lod',
                    storageBucket: 'x10d-lod.appspot.com',
                    messagingSenderId: '161243598641',
                    appId: '1:161243598641:web:08f4f934d98b305a13ea10',
                })
            } catch (e) {}

            firebase.auth().onAuthStateChanged((user) => {
                this.currentUser = user

                resolve()
            })
        })
    }
}
