import * as ExpoNotifications from 'expo-notifications'
import * as Permissions from 'expo-permissions'
import { Platform } from 'react-native'
import { RootNavigation } from '../../navigation'

export class Notifications {
    token: string

    constructor(navigationService: typeof RootNavigation) {
        ExpoNotifications.setNotificationHandler({
            handleNotification: async () => ({
                shouldShowAlert: true,
                shouldPlaySound: false,
                shouldSetBadge: false,
            }),
        })

        ExpoNotifications.addNotificationResponseReceivedListener((event) => {
            const { data } = event.notification.request.content

            navigationService.navigate('eventDetails', {
                id: data.id,
            })
        })
    }

    async setup(): Promise<void> {
        const { status } = await Permissions.getAsync(Permissions.NOTIFICATIONS)

        if (status !== 'granted') {
            await Permissions.askAsync(Permissions.NOTIFICATIONS)
        }

        this.token = (await ExpoNotifications.getExpoPushTokenAsync()).data

        if (Platform.OS === 'android') {
            await ExpoNotifications.setNotificationChannelAsync('default', {
                name: 'default',
                importance: ExpoNotifications.AndroidImportance.MAX,
                vibrationPattern: [0, 250, 250, 250],
                lightColor: '#FF231F7C',
            })
        }
    }
}
