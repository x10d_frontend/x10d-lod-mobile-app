export const palette = {
    black: '#202020',
    mirage: '#1B2734',
    shuttleGrey: '#56606A',
    alto: '#DBDBDB',
    alabaster: '#F9F9F9',
    aquamarineBlue: '#6AD1E3',

    white: '#ffffff',
    offWhite: '#e6e6e6',
    orange: '#FBA928',
    orangeDarker: '#EB9918',
    lightGrey: '#939AA4',
    lighterGrey: '#CDD4DA',
    angry: '#dd3333',
}
