import { formatWithOptions, parseISO } from 'date-fns/fp'
import { pipe } from 'ramda'
import { ru } from 'date-fns/locale'

// Общие функции
const parsedDate = parseISO
const dayOfYear = formatWithOptions({ locale: ru }, 'dd.MM.yyyy')

export const eventDate = pipe(parsedDate, dayOfYear)
