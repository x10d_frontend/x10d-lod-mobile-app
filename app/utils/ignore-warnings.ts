/**
 * Ignore some yellowbox warnings. Some of these are for deprecated functions
 * that we haven't gotten around to replacing yet.
 */
import { ignoreLogs } from 'react-native/Libraries/LogBox/LogBox'

ignoreLogs(['Require cycle:'])
