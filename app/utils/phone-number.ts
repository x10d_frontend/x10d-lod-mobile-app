import { parsePhoneNumber, AsYouType } from 'libphonenumber-js'

export function formatPhoneNumberAsYouType(phoneNumber: string): string {
    return new AsYouType().input(phoneNumber)
}

export function isPhoneNumberValid(phoneNumber: string): boolean {
    if (!phoneNumber) {
        return false
    }

    try {
        return parsePhoneNumber(phoneNumber).isValid()
    } catch (error) {
        return false
    }
}
